public class PositiveCounter {
    private int value;
    private static final int ONE = 1;
    private static final int ZERO = 0;

    private int numberOfDigitsDisplayed = 0;

    public PositiveCounter(int numberOfDigitsDisplayed) {
        this.numberOfDigitsDisplayed = numberOfDigitsDisplayed;
        this.reset();
    }

    public void increase() {
        this.value = Math.floorMod(this.value + ONE, (int) (Math.pow(10, numberOfDigitsDisplayed)));

    }

    public void setTo(int value) {
        this.value =  Math.floorMod(Math.max(value, 0), (int) (Math.pow(10, numberOfDigitsDisplayed)));
    }

    @Override
    final public String toString() {
        StringBuilder ret = new StringBuilder();
        if (value < 10)
            ret.append("00");
        else if (value < 100)
            ret.append("0");
        ret.append(Integer.toString(this.value));
        return ret.toString();
    }

    public void reset() {
        this.value = ZERO;
    }
}
