import javax.swing.*;

public class SapperGame extends JFrame {

    public static final int MAX_MAP_SIZE = 40;
    public static final int MIN_MAP_SIZE = 5;
    public static final int MAX_BOMBS_DENSITY = 2;

    private SapperGamePanel panel;
    private SapperOptionsFrame options;

    private SapperGame(int sizeX, int sizeY, int numberOfBombs) {
        this.initUI(sizeX, sizeY, numberOfBombs);
    }

    private SapperGame(int size, int numberOfBombs) {
        this(size, size, numberOfBombs);
    }

    private void initUI(int sizeX, int sizeY, int numberOfBombs) {
        this.panel = new SapperGamePanel(sizeX, sizeY, numberOfBombs, this);
        this.options = SapperOptionsFrame.getOptionsFrame(panel);
        JMenuBar menuBar = new SapperMenu(panel, options);

        this.setJMenuBar(menuBar);
        this.add(panel);

        this.setTitle("MineSweeper");
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);

        panel.resetBoard();
    }


    public static void main(String... args) {
        SapperGame game = new SapperGame(15, 20);

    }
}
