import java.util.*;

public class SapperBoard {
    private int sizeX;
    private int sizeY;
    private int numberOfBombs;
    private final static Random randomGenerator = new Random();

    private SapperField[][] boardFields;

    public SapperBoard(int sizeX, int sizeY, int numberOfBombs) {
        resetGameBoard(sizeX, sizeY, numberOfBombs);
    }

    public void resetGameBoard(int sizeX, int sizeY, int numberOfBombs) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.numberOfBombs = numberOfBombs;
        this.initializeFields();
    }

    private void initializeFields() {
        Set<IntegerPair> bombsCoordinates = new HashSet<>();
        this.boardFields = new SapperField[this.sizeX][this.sizeY];
        while (bombsCoordinates.size() < this.numberOfBombs) {
            IntegerPair integerPair = new IntegerPair(randomGenerator, this.sizeX, this.sizeY);
            if (bombsCoordinates.add(integerPair))
                this.boardFields[integerPair.getKey()][integerPair.getValue()] = new SapperBombField();

        }
        for (int x = 0; x < this.sizeX; x++)
            for (int y = 0; y < this.sizeY; y++)
                if (Objects.isNull(this.boardFields[x][y]))
                    this.boardFields[x][y] = new SapperNumberField();

        for (int x = 0; x < this.sizeX; x++)
            for (int y = 0; y < this.sizeY; y++)
                if (this.boardFields[x][y].hasValue())
                    this.boardFields[x][y].setFieldValue(this.countNeighbourBombs(x, y));
    }

    public void clickOnBoardField(int x, int y) throws LostGameException, WonGameException {
        if (!isOnBoard(x, y))
            throw new IllegalArgumentException("Coordinates x=" + x + " y=" + y + " are not on board");

        SapperField selectedField = this.boardFields[x][y];
        if (selectedField.canBeSetToVisible()) {
            if (selectedField.hasBomb())
                throw new LostGameException();

            if (selectedField.hasValue()) {
                selectedField.setVisible();
                if (selectedField.getFieldValue() == 0) {
                    this.markVisibleFrom(x, y);
                }
            }
            if (this.isWonGame())
                throw new WonGameException();
        }
    }

    private boolean isWonGame() {
        int areVisible = 0;
        for (SapperField[] boardFields: this.boardFields)
            for (SapperField boardField: boardFields)
                if (boardField.isVisible())
                    areVisible++;

        return areVisible == (sizeX * sizeY - numberOfBombs);
    }

    private boolean isOnBoard(int x, int y) {
        return x >= 0 && x < this.sizeX && y >= 0 && y < this.sizeY;
    }

    private int countNeighbourBombs(int x, int y) {
        if (!isOnBoard(x, y))
            throw new IllegalArgumentException("Bad coordinates to countNeighbourBombs function");

        int result = 0;
        for (int sx = -1; sx < 2; sx++) {
            for (int sy = -1; sy < 2; sy++) {
                int nx = sx + x, ny = sy + y;
                if (isOnBoard(nx, ny) && this.boardFields[nx][ny].hasBomb())
                    result ++;
            }
        }
        return result;
    }

    private void markVisibleFrom(int x, int y) {
        boolean[][] visited = new boolean[this.sizeX][this.sizeY];

        Stack<IntegerPair> stack = new Stack<>();
        stack.add(new IntegerPair(x, y));
        while (!stack.isEmpty()) {
            IntegerPair lastPair = stack.pop();
            this.boardFields[lastPair.getKey()][lastPair.getValue()].setVisible();
            visited[lastPair.getKey()][lastPair.getValue()] = true;
            for (int sx = -1; sx < 2; sx++) {
                for (int sy = -1; sy < 2; sy++) {
                    int nx = sx + lastPair.getKey(), ny = sy + lastPair.getValue();
                    if (isOnBoard(nx, ny) && this.boardFields[nx][ny].hasValue() && !visited[nx][ny]) {
                        this.boardFields[nx][ny].setVisible();
                        if (this.boardFields[nx][ny].getFieldValue() == 0)
                            stack.add(new IntegerPair(nx, ny));
                    }
                }
            }
        }
    }

    public int numberOfMarkedSupposedBombs() {
        int result = 0;
        for (int x = 0; x < this.sizeX; x++)
            for (int y = 0; y < this.sizeY; y++)
                if (!this.boardFields[x][y].canBeSetToVisible())
                    result++;

        return result;
    }

    public int numberOfBombs() {
        return this.numberOfBombs;
    }

    public boolean canBeSetToVisible(int x, int y) {
        if (isOnBoard(x, y))
            return this.boardFields[x][y].canBeSetToVisible();
        else throw new IllegalArgumentException("Bad coordinates in canBeSetToVisible function");
    }

    public  boolean hasBomb(int x, int y) {
        if (isOnBoard(x, y))
            return this.boardFields[x][y].hasBomb();
        else throw new IllegalArgumentException("Bad coordinates in hasBomb function");
    }

    public  boolean hasValue(int x, int y) {
        if (isOnBoard(x, y))
            return this.boardFields[x][y].hasValue();
        else throw new IllegalArgumentException("Bad coordinates in hasValue function");
    }

    public  boolean isVisible(int x, int y) {
        if (isOnBoard(x, y))
            return this.boardFields[x][y].isVisible();
        else throw new IllegalArgumentException("Bad coordinates in hasValue function");
    }

    public  void changeChangeAbility(int x, int y) {
        if (isOnBoard(x, y))
            this.boardFields[x][y].changeStatusChangeAbility(!this.boardFields[x][y].canBeSetToVisible());
        else throw new IllegalArgumentException("Bad coordinates in hasValue function");
    }

    public  int getValue(int x, int y) {
        if (isOnBoard(x, y))
            return this.boardFields[x][y].getFieldValue();
        else throw new IllegalArgumentException("Bad coordinates in getValue function");
    }

    public class LostGameException extends Exception {}

    public class WonGameException extends Exception {}
}
