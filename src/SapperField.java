public abstract class SapperField {

    private boolean isVisible;
    private boolean canBeSetToVisible;

    public SapperField() {
        this.isVisible = false;
        this.canBeSetToVisible = true;
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    public void setVisible() {
        if (this.canBeSetToVisible)
            this.isVisible = true;
    }

    public boolean canBeSetToVisible() {
        return canBeSetToVisible;
    }

    public void changeStatusChangeAbility(boolean canBeSetToVisible) {
        this.canBeSetToVisible = canBeSetToVisible;
    }

    public abstract boolean hasBomb();

    public abstract boolean hasValue();

    public abstract int getFieldValue();

    public abstract  void setFieldValue(int newFieldValue);

}
