import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class SapperMenu extends JMenuBar {

    private SapperGamePanel gamePanel;
    private SapperOptionsFrame optionsFrame;

    public SapperMenu(SapperGamePanel gamePanel, SapperOptionsFrame optionsFrame) {
        this.gamePanel = gamePanel;
        this.optionsFrame = optionsFrame;

        this.setBackground(Color.LIGHT_GRAY);

        JMenu gameMenu, otherMenu;
        JMenuItem newGameMenuItem, optionsMenuItem, exitMenuItem, aboutMenuItem;

        gameMenu = new JMenu("Game");
        gameMenu.setMnemonic(KeyEvent.VK_G);
        this.add(gameMenu);

        newGameMenuItem = new JMenuItem("New Game");
        newGameMenuItem.setMnemonic(KeyEvent.VK_N);
        newGameMenuItem.addActionListener(actionEvent -> this.gamePanel.resetBoard());
        gameMenu.add(newGameMenuItem);

        gameMenu.addSeparator();

        optionsMenuItem = new JMenuItem("Options");
        optionsMenuItem.setMnemonic(KeyEvent.VK_O);
        optionsMenuItem.addActionListener(actionEvent -> this.optionsFrame.showOptions());
        gameMenu.add(optionsMenuItem);

        gameMenu.addSeparator();

        exitMenuItem = new JMenuItem("Exit game");
        exitMenuItem.setMnemonic(KeyEvent.VK_E);
        exitMenuItem.addActionListener(actionEvent -> System.exit(0));
        gameMenu.add(exitMenuItem);

        otherMenu = new JMenu("Other");
        otherMenu.setMnemonic(KeyEvent.VK_T);
        this.add(otherMenu);

        aboutMenuItem = new JMenuItem("About");
        aboutMenuItem.setMnemonic(KeyEvent.VK_A);
        aboutMenuItem.addActionListener(actionEvent ->
                JOptionPane.showMessageDialog(this.gamePanel, "Author: Maciej Procyk", "About",
                        JOptionPane.INFORMATION_MESSAGE, null));
        otherMenu.add(aboutMenuItem);
    }
}
