import javax.swing.*;
import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

class SapperGamePanel extends JPanel {

    private static final String resourcesPath = "resources/";

    public static final int FIELD_SIZE = 24;
    private static final int EXTRA_HEIGHT_SIZE = 75;
    private static final int START_BOARD_PANEL_Y_LINE = 1;
    private static final int UP_BOARD_SIZE = 15;
    private static final int ONE_SECOND = 1000;
    private static final int DIGITS_DISPLAYED = 3;

    private int sizeX;
    private int sizeY;
    private int numberOfBombs;

    private PositiveCounter timeCount;
    private PositiveCounter flagsCount;
    private Timer timer;

    private final static Object[] dialogOptions = {"Reset game", "Exit"};

    private SapperBoard currentGameBoard;
    private SapperFieldButton[][] buttons;
    private final SapperGame game;

    private JLabel flagsNumberLabel;
    private static final Font countersFont;

    static {
        Font tempFont = null;
        try {
//            tempFont = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(resourcesPath + "font.ttf"));
            // TODO change for artifact build
            // For artifact build use
            tempFont = Font.createFont(Font.TRUETYPE_FONT, SapperGame.class.getResourceAsStream(resourcesPath + "font.ttf"));
        } catch (FontFormatException | IOException exc) {
            System.out.println(exc.getMessage());
            System.exit(0);
        }
        countersFont = tempFont.deriveFont(Font.PLAIN, 40);
    }

    public SapperGamePanel(int sizeX, int sizeY, int numberOfBombs, SapperGame game) {
        this.game = game;
        this.timeCount = new PositiveCounter(DIGITS_DISPLAYED);
        this.flagsCount = new PositiveCounter(DIGITS_DISPLAYED);
        this.setBackground(Color.LIGHT_GRAY);
        this.initiateNewGame(sizeX, sizeY, numberOfBombs);
    }

    public void initiateNewGame(int sizeX, int sizeY, int numberOfBombs) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.numberOfBombs = numberOfBombs;
        this.initPanel();
    }

    public void hideGameFrame() {
        this.game.setVisible(false);
    }

    public void showGameFrame() {
        this.game.setVisible(true);
    }

    public int getSizeX() {
        return this.sizeX;
    }

    public int getSizeY() {
        return this.sizeY;
    }

    public int getNumberOfBombs() {
        return this.numberOfBombs;
    }

    public void actualizeNumberOfMarkedBombs() {
        flagsCount.setTo(currentGameBoard.numberOfBombs() - currentGameBoard.numberOfMarkedSupposedBombs());
        flagsNumberLabel.setText(flagsCount.toString());
    }

    private void initPanel() {
        currentGameBoard = new SapperBoard(sizeX, sizeY, numberOfBombs);

        this.removeAll();
        this.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        this.setLayout(new GridBagLayout());

        JLabel timeCounterLabel = new JLabel("000");
        timeCounterLabel.setFont(countersFont);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.LINE_END;
        constraints.gridwidth = sizeX / 2;
        constraints.ipady = UP_BOARD_SIZE;
        constraints.gridx = sizeX / 2;
        constraints.gridy = 0;

        if (timer != null)
            timer.stop();

        timer = new Timer(ONE_SECOND, actionEvent -> {
            timeCounterLabel.setText(timeCount.toString());
            timeCount.increase();
        });
        timer.start();

        this.add(timeCounterLabel, constraints);

        flagsNumberLabel = new JLabel("000");
        flagsNumberLabel.setFont(countersFont);
        constraints.anchor = GridBagConstraints.LINE_START;
        constraints.gridwidth = sizeX / 2;
        constraints.ipady = UP_BOARD_SIZE;
        constraints.gridx = 1;
        constraints.gridy = 0;

        this.add(flagsNumberLabel, constraints);

        constraints.anchor = GridBagConstraints.CENTER;
        constraints.gridwidth = 1;
        constraints.weightx = 0.1;
        constraints.weighty = 0.1;


        buttons = new SapperFieldButton[sizeX][sizeY];
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                constraints.gridx = x;
                constraints.gridy = START_BOARD_PANEL_Y_LINE + y;
                buttons[x][y] = new SapperFieldButton(x, y, this);
                this.add(buttons[x][y], constraints);
            }
        }
        this.setSize(FIELD_SIZE * sizeX, FIELD_SIZE * sizeY);
        game.setSize(FIELD_SIZE * sizeX, FIELD_SIZE * sizeY + EXTRA_HEIGHT_SIZE);
        this.resetBoard();
    }

    public void clickOnGameBoard(int x, int y) throws SapperBoard.LostGameException, SapperBoard.WonGameException {
        this.currentGameBoard.clickOnBoardField(x, y);
    }

    public boolean hasBomb(int x, int y) {
        return this.currentGameBoard.hasBomb(x, y);
    }

    public void resetBoard() {
        this.timeCount.reset();
        this.timer.start();
        currentGameBoard.resetGameBoard(sizeX, sizeY, numberOfBombs);
        this.flagsCount.setTo(currentGameBoard.numberOfBombs() - currentGameBoard.numberOfMarkedSupposedBombs());
        this.actualizeNumberOfMarkedBombs();
        for (SapperFieldButton[] buttons: buttons)
            for (SapperFieldButton button: buttons)
                button.resetButtonState();

        this.actualizeButtonStates();
    }

    public void actualizeButtonStates(int changedX, int changedY) {
        actualizeButtonStates(changedX, changedY, false);
    }

    public void actualizeButtonStates() {
        actualizeButtonStates(0, 0, true);
    }

    private void actualizeButtonStates(int changedX, int changedY, boolean actualizeAll) {
        if (actualizeAll) {
            for (int x = 0; x < sizeX; x++) {
                for (int y = 0; y < sizeY; y++) {
                    if (!this.currentGameBoard.canBeSetToVisible(x, y))
                        this.buttons[x][y].setFlag();
                    else if (!this.currentGameBoard.isVisible(x, y))
                        this.buttons[x][y].setHidden();
                    else if (this.currentGameBoard.hasBomb(x, y)) {
                        this.buttons[x][y].setBomb();
                        if (this.buttons[x][y].isEnabled())
                            this.buttons[x][y].setEnabled(false);
                    }
                    else if (this.currentGameBoard.hasValue(x, y)) {
                        this.buttons[x][y].setValue(this.currentGameBoard.getValue(x, y));
                        if (this.buttons[x][y].isEnabled())
                            this.buttons[x][y].setEnabled(false);
                    }
                }
            }
            return;
        }

        if (!this.currentGameBoard.canBeSetToVisible(changedX, changedY))
            this.buttons[changedX][changedY].setFlag();
        else if (!this.currentGameBoard.isVisible(changedX, changedY))
            this.buttons[changedX][changedY].setHidden();
        else if (this.currentGameBoard.hasValue(changedX, changedY))
            actualizeButtonStates();
    }

    public void changeChangeAbility(int x, int y) {
        this.currentGameBoard.changeChangeAbility(x, y);
    }

    public void clearBoardAndShow() {
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                if (this.currentGameBoard.hasBomb(x, y))
                    this.buttons[x][y].setBomb();
                else if (this.currentGameBoard.hasValue(x, y))
                    this.buttons[x][y].setValue(this.currentGameBoard.getValue(x, y));
            }
        }
    }

    public void finishLostGame() {
        this.decideDialog("You lost the game.");
    }

    public void finishWonGame() {
        this.decideDialog("You won the game.");
    }

    private void decideDialog(String information) {
        this.clearBoardAndShow();
        timer.stop();
        int decision = this.showEndOfGameDialog(information);
        if (decision == 1)
            System.exit(0);
        this.resetBoard();
    }

    private int showEndOfGameDialog(String information) {
        return JOptionPane.showOptionDialog(this, information + " What would you like to do?", "End of game",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, dialogOptions, dialogOptions[0]);
    }
}
