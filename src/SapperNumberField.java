public class SapperNumberField extends SapperField{
    private int numberOfBombs;

    @Override
    public boolean hasValue() {
        return true;
    }

    @Override
    public boolean hasBomb() {
        return false;
    }

    @Override
    public int getFieldValue() {
        return this.numberOfBombs;
    }

    @Override
    public void setFieldValue(int newFieldValue) {
        this.numberOfBombs = newFieldValue;
    }
}
