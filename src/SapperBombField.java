public class SapperBombField extends SapperField {

    @Override
    public boolean hasBomb() {
        return true;
    }

    @Override
    public boolean hasValue() {
        return false;
    }

    @Override
    public int getFieldValue() {
        return 0;
    }

    @Override
    public void setFieldValue(int newFieldValue) {  }
}
