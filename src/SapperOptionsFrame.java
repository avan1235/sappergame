import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;

public class SapperOptionsFrame extends JFrame {

    private static SapperOptionsFrame frame = null;
    private static boolean cheatsEnabled = false;

    private SapperGamePanel gamePanel;
    private static JFormattedTextField sizeField, numberBombsField;

    static {
        NumberFormatter formatterSizeMap, formatterSizeBombs;
        formatterSizeMap = new NumberFormatter(NumberFormat.getInstance());
        formatterSizeMap.setValueClass(Integer.class);
        formatterSizeMap.setMinimum(1);
        formatterSizeMap.setMaximum(SapperGame.MAX_MAP_SIZE);
        formatterSizeMap.setAllowsInvalid(false);

        formatterSizeBombs = new NumberFormatter(NumberFormat.getInstance());
        formatterSizeBombs.setValueClass(Integer.class);
        formatterSizeBombs.setMinimum(1);
        formatterSizeBombs.setMaximum(SapperGame.MAX_MAP_SIZE * SapperGame.MAX_MAP_SIZE / SapperGame.MAX_BOMBS_DENSITY);
        formatterSizeBombs.setAllowsInvalid(false);

        sizeField = new JFormattedTextField(formatterSizeMap);
        numberBombsField = new JFormattedTextField(formatterSizeBombs);

        sizeField.setColumns(4);
        numberBombsField.setColumns(4);
    }

    private SapperOptionsFrame() { }

    private void initUI() {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        panel.setLayout(new FlowLayout(FlowLayout.LEADING));

        panel.add(new JLabel("Board size:"));
        panel.add(sizeField);

        JLabel bombsLabel = new JLabel("Number of bombs:");
        bombsLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {
                    if (cheatsEnabled) CheatsPanel.getInstance().disableCheats();
                    else CheatsPanel.getInstance().enableChats();
                    cheatsEnabled = !cheatsEnabled;
                }
            }
        });
        panel.add(bombsLabel);
        panel.add(numberBombsField);

        panel.setBackground(Color.LIGHT_GRAY);

        frame.add(panel);

        frame.setResizable(false);
        frame.setTitle("Options");
        frame.setDefaultCloseOperation(HIDE_ON_CLOSE);
        frame.pack();

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                int newSize, newBombs;

                try {
                    newSize = Integer.parseInt(sizeField.getText());
                    newBombs = Integer.parseInt(numberBombsField.getText());
                } catch (NumberFormatException e) {
                    newSize = gamePanel.getSizeX();
                    newBombs = gamePanel.getNumberOfBombs();
                }

                if (newSize < SapperGame.MIN_MAP_SIZE)
                    newSize = SapperGame.MIN_MAP_SIZE;

                if (newBombs > newSize * newSize / SapperGame.MAX_BOMBS_DENSITY)
                    newBombs = newSize * newSize / SapperGame.MAX_BOMBS_DENSITY;

                gamePanel.hideGameFrame();
                gamePanel.initiateNewGame(newSize, newSize, newBombs);
                gamePanel.resetBoard();
                getOptionsFrame(gamePanel).setVisible(false);
                gamePanel.showGameFrame();
            }
        });

    }

    public void showOptions() {
        frame.setLocationRelativeTo(gamePanel);
        frame.setVisible(true);
        sizeField.setValue(gamePanel.getSizeX());
        numberBombsField.setValue(gamePanel.getNumberOfBombs());
    }

    public static SapperOptionsFrame getOptionsFrame(SapperGamePanel gamePanel) {
        if (frame == null) {
            frame = new SapperOptionsFrame();
            frame.gamePanel = gamePanel;
            frame.initUI();
        }
        return frame;
    }
}
