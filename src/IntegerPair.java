import java.util.Objects;
import java.util.Random;

public class IntegerPair {
    private int key;
    private int value;

    public IntegerPair(int key, int value) {
        this.key = key;
        this.value = value;
    }

    public IntegerPair(Random generator, int modK, int modV) {
        this.key = generator.nextInt(modK);
        this.value = generator.nextInt(modV);
    }

    public int getKey() {
        return this.key;
    }

    public int getValue() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerPair that = (IntegerPair) o;
        return key == that.key && value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }
}

