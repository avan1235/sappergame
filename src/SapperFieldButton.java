import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.IOException;

class SapperFieldButton extends JButton {

    private static final String resourcesPath = "resources/";
    private static final int BUTTON_SIZE = SapperGamePanel.FIELD_SIZE-1;

    private static final Icon hiddenIcon;
    private static final Icon flagIcon;
    private static final Icon bombIcon;
    private static final Icon[] numbersIcons = new Icon[9];

    static {

        Image tempHidden = null, tempFlag = null, tempBomb = null;
        Image[] numbers = new Image[9];
        try {
//            tempHidden = ImageIO.read(new FileInputStream(resourcesPath + "hidden.jpg"));
//            tempFlag = ImageIO.read(new FileInputStream(resourcesPath + "flag.jpg"));
//            tempBomb = ImageIO.read(new FileInputStream(resourcesPath + "bomb.jpg"));
//            for (int i = 0; i < 9; i++)
//                numbers[i] = ImageIO.read(new FileInputStream(resourcesPath + i + ".jpg"));

            // TODO change for artifact build
            // For artifact build use
            tempHidden = ImageIO.read(SapperGame.class.getResource(resourcesPath + "hidden.jpg"));
            tempFlag = ImageIO.read(SapperGame.class.getResource(resourcesPath + "flag.jpg"));
            tempBomb = ImageIO.read(SapperGame.class.getResource(resourcesPath + "bomb.jpg"));
            for (int i = 0; i < 9; i++)
                numbers[i] = ImageIO.read(SapperGame.class.getResource(resourcesPath + i + ".jpg"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }

        Icon tempIcon;
        tempIcon = new ImageIcon(tempHidden.getScaledInstance(BUTTON_SIZE, BUTTON_SIZE,0));
        hiddenIcon = tempIcon;
        tempIcon = new ImageIcon(tempFlag.getScaledInstance(BUTTON_SIZE, BUTTON_SIZE,0));
        flagIcon = tempIcon;
        tempIcon = new ImageIcon(tempBomb.getScaledInstance(BUTTON_SIZE, BUTTON_SIZE,0));
        bombIcon = tempIcon;
        for (int i = 0; i < 9; i++) {
            tempIcon = new ImageIcon(numbers[i].getScaledInstance(BUTTON_SIZE, BUTTON_SIZE,0));
            numbersIcons[i] = tempIcon;
        }
    }

    private final int xPosition;
    private final int yPosition;

    private final SapperGamePanel game;

    public SapperFieldButton(int xPosition, int yPosition, SapperGamePanel gamePanel) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.game = gamePanel;

        this.setBackground(Color.LIGHT_GRAY);

        this.addActionListener(actionEvent -> {
            try {
                this.game.clickOnGameBoard(this.xPosition, this.yPosition);
                this.game.actualizeButtonStates(this.xPosition, this.yPosition);
            } catch (SapperBoard.LostGameException e) {
                this.game.finishLostGame();
            } catch (SapperBoard.WonGameException e) {
                this.game.finishWonGame();
            }
        });

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    if (isEnabled()) {
                        game.changeChangeAbility(xPosition, yPosition);
                        game.actualizeButtonStates(xPosition, yPosition);
                        gamePanel.actualizeNumberOfMarkedBombs();
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (game.hasBomb(xPosition, yPosition))
                    CheatsPanel.getInstance().setBackground(Color.WHITE);
                else
                    CheatsPanel.getInstance().setBackground(Color.BLACK);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                CheatsPanel.getInstance().setBackground(Color.BLACK);
            }
        });
    }

    private void setIconImage(Icon icon) {
        this.setIcon(icon);
        this.setDisabledIcon(icon);
    }

    public void setHidden() {
        this.setIconImage(hiddenIcon);
    }

    public void setFlag() {
        this.setIconImage(flagIcon);
    }

    public void setBomb() {
        this.setIconImage(bombIcon);
    }

    public void setValue(int value) {
        this.setIconImage(numbersIcons[value]);
    }

    public void resetButtonState() {
        this.setHidden();
        this.setEnabled(true);
    }
}
