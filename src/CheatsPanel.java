import javax.swing.*;
import java.awt.*;

class CheatsPanel extends JDialog {

    private static CheatsPanel frame = null;
    private JPanel panel;

    private CheatsPanel() {
        panel = new JPanel();
        panel.setBackground(Color.BLACK);
        this.add(panel);

        this.setLayout(new GridBagLayout());

        this.setUndecorated(true);
        this.setSize(1,1);
        this.setType(Type.UTILITY);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setAlwaysOnTop(true);
        this.setVisible(false);
    }

    public static CheatsPanel getInstance() {
        if (frame == null)
            frame = new CheatsPanel();
        return frame;
    }

    public void setBackground(Color color) {
        if (this.panel != null)
            this.panel.setBackground(color);
    }

    public void disableCheats() {
        this.setVisible(false);
    }

    public void enableChats() {
        this.setVisible(true);
    }
}
